<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\CategoryDuplicator\Block\Adminhtml\Category\Duplicate\Edit;

/**
 * Adminhtml cms block edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('duplicate_form');
        $this->setTitle(__('Duplicate Category'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $fieldset = $form->addFieldset(
            'base_fieldset',
             ['class' => 'fieldset-wide duplicate-fieldset']
        );
        $form->setHtmlIdPrefix('block_');
        $fieldset->addField(
            'copy_product_relations',
            'select',
            [
                'name'  => 'copy_product_relations',
                'label' => __('Copy Product Relations'),
                'title' => __('Copy Product Relations'),
                'values'=>[
                    ['value' => 0,'label' => __('No')],
                    ['value' => 1,'label' => __('Yes')],
                ]
            ]
        );

        $fieldset->addField(
            'include_subcategories',
            'select',
            [
                'name'  => 'include_subcategories',
                'label' => __('Include Subcategories'),
                'title' => __('Include Subcategories'),
                'values'=>[
                    ['value' => 0,'label' => __('No')],
                    ['value' => 1,'label' => __('Yes')],
                ]
            ]
        );

        $fieldset->addField(
            'selected_category',
            'hidden',
            [
                'name' => 'selected_category'
            ]
        );

        $fieldset->addField(
            'duplicate_category',
            'hidden',
            [
                'name' => 'duplicate_category',
                'value' => $id,
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}

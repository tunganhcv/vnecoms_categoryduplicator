<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\CategoryDuplicator\Block\Adminhtml\Category;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Catalog\Block\Adminhtml\Category\AbstractCategory;


class DuplicateCategoryButton extends AbstractCategory implements ButtonProviderInterface
{
    /**
     * Delete button
     *
     * @return array
     */
    public function getButtonData()
    {
        $id = $this->getRequest()->getParam('id');
        return [
            'label' => __('Duplicate Category'),
            'class' => 'save',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 10,
            'on_click' => sprintf("location.href = '%sid/%s'", $this->getUrl('category/duplicate/index'), $id)
        ];
    }
}

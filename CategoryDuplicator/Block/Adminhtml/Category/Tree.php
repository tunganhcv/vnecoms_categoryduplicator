<?php

namespace Vnecoms\CategoryDuplicator\Block\Adminhtml\Category;

use Magento\Framework\Data\Tree\Node;

class Tree extends \Magento\Catalog\Block\Adminhtml\Category\Tree
{
    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('duplicate/category/tree.phtml');
    }
}

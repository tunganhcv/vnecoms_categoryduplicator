<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\CategoryDuplicator\Controller\Adminhtml\Duplicate;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;

class Duplicate extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * Duplicate constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        ResourceConnection $resourceConnection
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
        $this->_categoryFactory = $categoryFactory;
        $this->_productFactory = $productFactory;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $categorySelected = $this->getRequest()->getParam('selected_category');
        if (is_string($categorySelected)) {
            $categoryData = explode(', ', $categorySelected);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $duplicateCategoryId = $this->getRequest()->getParam('duplicate_category');

        foreach ($categoryData as $parentCategoryId) {
            /*If not select parent category*/
            if (!$parentCategoryId) {
                $this->messageManager->addError('Please select parent category');
                return $resultRedirect->setPath('category/*/', ['_current' => true, 'id' => $duplicateCategoryId]);
            }
            /*Add duplicate category to parent category */
            $category = $this->_categoryFactory->create()->load($duplicateCategoryId);
            $duplicateCategory = clone $category;
            $parentCategory = $this->_categoryFactory->create()->load($parentCategoryId);
            $childCategory = $this->_categoryFactory->create()->load($duplicateCategoryId)->getChildrenCategories();
            $this->addSubcategory($parentCategory, $duplicateCategory);
            /*Add all subcategories to duplicate category*/
            if ($this->getRequest()->getParam('include_subcategories') == 1) {
                $this->addIncludeSubcategories($childCategory, $duplicateCategory);
            }
        }
        $this->messageManager->addSuccess('Category has been duplicated');
        return $resultRedirect->setPath('catalog/category/index', ['_current' => true, 'id' => null]);
    }

    /**
     * @param $parentCategory
     * @param $subCategory
     * Add subcategory
     */
    function addSubcategory($parentCategory, $subCategory)
    {
        $products = $subCategory->getProductsPosition();
        $subCategory->setId(null);
        $subCategory->setLevel(null);
        $subCategory->setName('New ' . $subCategory->getData('name'));
        $subCategory->setPath($parentCategory->getPath());
        $subCategory->setParentId($parentCategory->getId());
        $subCategory->setUrlKey($subCategory->getUrlKey() . rand(1000, 9999));
        $subCategory->save();

        /*Add all relation products to duplicate category*/
        if ($this->getRequest()->getParam('copy_product_relations') == 1 && count($products) > 0) {
            $cate = $subCategory->getId();
            $connection = $this->resourceConnection->getConnection();
            $rows = [];
            foreach ($products as $productId => $position) {
                $rows[] = [
                    'category_id'   => $cate,
                    'product_id'    => $productId,
                    'position'      => $position,
                ];
            }
            $connection->insertMultiple(
                $this->resourceConnection->getTableName('catalog_category_product'),
                $rows
            );
        }

    }

    /**
     * @param $category
     * @param $duplicateCategory
     * Add include subcategories
     */
    function addIncludeSubcategories($childCategory, $duplicateCategory)
    {
        foreach ($childCategory as $subCategory) {
            $subCategory = $this->_categoryFactory->create()->load($subCategory->getId());
            $childCategories = $this->_categoryFactory->create()->load($subCategory->getId())->getChildrenCategories();
            $copy = clone $subCategory;
            $this->addSubcategory($duplicateCategory, $copy);
            if (count($childCategories) > 0) {
                $this->addIncludeSubcategories($childCategories, $copy);
            }
        }
    }
}

